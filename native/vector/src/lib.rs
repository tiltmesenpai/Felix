#[macro_use] extern crate rustler;
#[macro_use] extern crate lazy_static;
extern crate num;

use rustler::{NifEnv, NifTerm, NifResult, NifEncoder};
use rustler::types::{NifBinary,OwnedNifBinary};
use num::traits::Float;

mod atoms {
    rustler_atoms! {
        atom rust;
    }
}

rustler_export_nifs! {
    "Elixir.Vector",
    [ ("add", 2, add)
    , ("cardinality", 1, cardinality)
    , ("dist", 2, dist)
    , ("dot", 2, dot)
    , ("from_list", 1, list_to_bin)
    , ("mode", 0, mode)
    , ("scale", 2, scale)
    , ("to_list", 1, bin_to_list)
    , ("z_order", 2, z_order)
    ],
    None
}

fn mode<'a>(env: NifEnv<'a>, _args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    Ok(atoms::rust().encode(env))
}

fn dist_2(a: Vector, b: Vector) -> f32 {
    return a.iter().zip(b.iter()).map(|(x, y)| (x - y).powi(2)).fold(0.0, |sum, i| sum + i);
}

fn scale_v(xs: Vector, i:f32) -> Vector{
    let mut bytes: Vector = [0.0;300];
    for (ix, x) in xs.iter().enumerate(){
        bytes[ix] = i*x;
    }
    bytes
}

fn add_v(xs: Vector, ys: Vector) -> Vector{
    let mut bytes: Vector = [0.0;300];
    for(i, (x, y)) in xs.iter().zip(ys.iter()).enumerate(){
        bytes[i] = x + y;
    }
    bytes
}

fn z_order<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    let ys: Vector = from_binary(try!(args[1].decode()));
    let mut n : i32 = 0;
    let mut dim = 0;
    for (ix, (x, y)) in xs.iter().zip(ys.iter()).enumerate() {
        let v = xor_msb(*x, *y);
        if n < v {
            n = v;
            dim = ix;
        }
    }
    Ok((xs[dim] - ys[dim]).encode(env))
}

fn xor_msb(a: f32, b: f32) -> i32 {
    static DE_BRUJIN_BITS : [i32; 32] = 
        [  0,  9,  1, 10, 13, 21,  2, 29, 11, 14, 16, 18, 22, 25,  3, 30
        ,  8, 12, 20, 28, 15, 17, 24,  7, 19, 27, 23,  6, 26,  5,  4, 31];
    let (x, x_ex, _xsi) = Float::integer_decode(a);
    let (y, y_ex, _ysi) = Float::integer_decode(b);
    if x_ex != y_ex{
        use std::cmp;
        cmp::max(x_ex, y_ex) as i32
    }
    else{
        let mut v = (x ^ y) as u32;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        (x_ex as i32) - DE_BRUJIN_BITS[(v.wrapping_mul(0x077CB531) >> 27) as usize]
    }
}

fn dot<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    let ys: Vector = from_binary(try!(args[1].decode()));
    Ok(xs.iter().zip(ys.iter()).map(|(x, y)| x * y).fold(0.0, |sum, x| sum + x).encode(env))
}

fn scale<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    let s: f32 = try!(args[1].decode());
    let r: NifBinary = to_binary(scale_v(xs, s)).release(env);
    Ok(r.encode(env))
}
fn cardinality<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    Ok(xs.iter().fold(0.0, |sum, i| sum + i.powi(2)).sqrt().encode(env))
}

fn dist<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    let ys: Vector = from_binary(try!(args[1].decode()));
    Ok(dist_2(xs, ys).sqrt().encode(env))
}

fn add<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs: Vector = from_binary(try!(args[0].decode()));
    let ys: Vector = from_binary(try!(args[1].decode()));
    let r:  NifBinary = to_binary(add_v(xs, ys)).release(env);
    Ok(r.encode(env))
}
use std::ops::Deref;
type Vector = [f32;300];

fn from_binary(xs:NifBinary) -> Vector {
    if xs.len() != 1200 {
        [0.0;300]
    }
    else{
        unsafe{
            let mut bytes : [u8;1200] = [0;1200];
            bytes.copy_from_slice(xs.deref());
            std::mem::transmute::<[u8;1200],Vector>(bytes)
        }
    }
}

fn from_list(xs: Vec<f32>) -> Vector {
    use std::cmp;
    let mut vec = [0.0;300];
    for i in 0..cmp::min(xs.len(), 300){
        vec[i] = xs[i];
    }
    vec
}

fn to_binary(xs:Vector) -> OwnedNifBinary{
    let mut out = OwnedNifBinary::new(1200).expect("Could not allocate binary");
    let bytes : [u8;1200] = unsafe { std::mem::transmute::<Vector,[u8;1200]>(xs) };
    for (i, x) in out.iter_mut().enumerate(){
        *x = bytes[i];
    }
    out
}

fn list_to_bin<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs : Vec<f32> = try!(args[0].decode());
    Ok(to_binary(from_list(xs)).release(env).encode(env))
}

fn bin_to_list<'a>(env: NifEnv<'a>, args: &[NifTerm<'a>]) -> NifResult<NifTerm<'a>> {
    let xs : NifBinary = try!(args[0].decode());
    let v  : Vec<f32> = from_binary(xs).iter().cloned().collect();
    Ok(v.encode(env))
}
