defmodule ExCheck.CheckVector do
  use ExUnit.Case, async: false
  use ExCheck
  alias Vector

  property :add_commutative do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      Vector.dist(Vector.add(a, b), Vector.add(b, a)) < 1.0e-8
    end
  end

  property :add_associative do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      c = Vector.random_vector
      Vector.dist(Vector.add(Vector.add(a, b), c), Vector.add(a, Vector.add(b, c))) < 1.0e-8
    end
  end

  property :add_distributive do
    for_all x in float() do
      a = Vector.random_vector
      b = Vector.random_vector
      Vector.dist(Vector.scale(Vector.add(a, b), x), Vector.add(Vector.scale(a, x), Vector.scale(b, x))) < 1.0e-8
    end
  end

  property :add_triangle do
    for_all _x in int() do
      a = Vector.random_vector
      b = Vector.random_vector
      Vector.cardinality(a) + Vector.cardinality(b) > Vector.dist(a, b)
    end
  end
end
