defmodule Mix.Tasks.MakeDb do
  use Mix.Task
  @zeroes Vector.from_list(for _ <- 1..300, do: 0.0)

  def run(_) do
    file = Application.fetch_env!(:felix, :corpus_path)
    File.rm(file <> ".vertices.dets")
    {:ok, table} = :dets.open_file(file <> ".vertices.dets", [estimated_no_objects: 400001, ram_file: true])
    :dets.insert(table, {"<HEAD>", {@zeroes, 9, nil, nil}})
    File.open(file <> ".dat", [:read], fn file ->
      IO.stream(file, :line)
      |> Stream.map(&String.strip/1)
      |> Stream.map(&String.split(&1, " "))
      |> Stream.map(&parse_line/1)
      |> Stream.each(&(&1 |> VP.insert(table)))
      |> track_rate
      |> Stream.run
    end)
    :ok = :dets.close(table)
    IO.puts "OK"
  end

  defp track_rate(stream) do
    Stream.scan(stream, {:os.system_time(:millisecond), 0}, fn(_, {last, count}) ->
      if :os.system_time(:millisecond) - last > 1000 do
        IO.puts("Rate: " <> inspect(count) <> "/s")
        {:os.system_time(:millisecond), 0}
      else
        {last, count + 1}
      end
    end)
  end


  defp parse_line([token | nums]) do
    {token, Enum.map(nums, fn num ->
      case Float.parse(num) do
        :error ->
          IO.inspect num
          IO.puts "ERROR"
          0
        {x, _} -> x
      end
    end
    ) |> Vector.from_list}
  end
end
