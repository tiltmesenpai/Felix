defmodule Glove do
  use GenServer

  @zeroes for _ <- 1..300, do: 0

  def start_link do
    GenServer.start_link(__MODULE__, db_path)
  end

  def vec_for(server, word) do
    GenServer.call(server, {:vector, word})
  end

  def diff_words(server, word_a, word_b) do
    GenServer.call(server, {:diff, word_a, word_b})
  end

  def all(server) do
    GenServer.call(server, :all)
  end

  def nearest_node(server, vector) do
    GenServer.call(server, {:node, vector})
  end

  def nearest_neighbor(server, word) do
    GenServer.call(server, {:neighbor, word})
  end

  def db(server) do
    GenServer.call(server, :db)
  end

  defp db_path() do
    Application.fetch_env!(:felix, :corpus_path) <> ".vertices.dets"
  end

  defp next(db, a) do
    case :dets.lookup(db, a) do
      [next] -> next
      _ -> next(db, :dets.next(db, a))
    end
  end

  def init(path) do
    {:ok, db} = :dets.open_file(path, [access: :read])
    {:ok, db}
  end

  def handle_call(:db, _from, db) do
    {:reply, db, db}
  end

  def handle_call({:vector, words}, _from, db) do
    {:reply, _vec_for(db, words), db}
  end

  def handle_call({:diff, word_a, word_b}, _from, db) do
    {:reply, _compare_words(db, word_a, word_b), db}
  end

  def handle_call(:all, _from, db) do
    {:reply, Stream.unfold(:dets.first(db), &next(db, &1) |> Tuple.duplicate(2)), db}
  end

  def terminate(_reason, db) do
    :dets.close(db)
  end

  defp _compare_words(db, a, b) do
    vec_a = _vec_for(db, a)
    vec_b = _vec_for(db, b)
    Vector.dist(vec_a, vec_b)
  end

  defp _vec_for(db, word) do
    String.split(word, " ")
          |> Enum.map(&String.strip/1)
          |> Enum.map(fn(word) ->
              case :dets.lookup(db, word) do
                [{^word, {vec, _limit, _left, _right}}] -> vec
                _ -> @zeroes
              end
            end )
          |> Enum.reduce(&Vector.add/2)
  end
end
